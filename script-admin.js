"use strict";

const gBASE_URL_PETS = "https://pucci-mart.onrender.com/api/pets/";

var vPetTable = "";
var vStt = 1;
var vCurrentPetId = Number;

// CRUD
const CREATE = "create";
const UPDATE = "update";
const DELETE = "delete";

$(function () {
  createPetTable();
});

/******** Funtions help load Data To Table ********/

function createPetTable() {
  vPetTable = $("#pet-table").DataTable({
    columns: [
      {
        defaultContent: 1,
        render: () => vStt++,
      },
      {
        data: "name",
      },
      {
        data: "type",
      },
      {
        data: "description",
      },
      {
        data: "imageUrl",
        render: (data) => `<img class='img-pet' src=${data}>`,
      },
      {
        data: "price",
      },
      {
        data: "promotionPrice",
      },
      {
        data: null,
        render: renderButtons,
      },
    ],
    columnDefs: [{ targets: "_all", class: "text-center" }],
  });
  loadPet();
}

async function getAllPet() {
  const response = await fetch(gBASE_URL_PETS);
  const data = await response.json();
  const allPets = data.rows;
  return allPets;
}

async function loadPet() {
  var vAllPets = await getAllPet();
  var filterPet = vAllPets.filter(
    (pet) => pet.price <= vSliderValue[1] && pet.price >= vSliderValue[0]
  );
  vStt = 1;
  vPetTable.clear();
  vPetTable.rows.add(filterPet);
  vPetTable.draw();
}

getAllPet();
function renderButtons(paramData) {
  var vBtnEdit = `<i data-pet='${JSON.stringify(
    paramData
  )}' class="edit-pet fa-solid fa-pen-to-square fa-lg
    text-primary user-select-all mr-2" style="cursor:pointer;"></i>`;
  var vBtnDelete = `<i data-id='${paramData.id}' class="delete-pet fa-regular fa-trash-can fa-lg
    text-danger user-select-all" style="cursor:pointer;"></i>`;
  return `${vBtnEdit} ${vBtnDelete}`;
}

/******** Event Handler ********/

// Update Pet
$("#pet-table").on("click", ".edit-pet", function () {
  var vPetObj = JSON.parse(this.dataset.pet);
  handleUpdateBtn(vPetObj);
});

$("#btn-update-pet").click(() => handleCrud(UPDATE));

// Delete Pet
$("#pet-table").on("click", ".delete-pet", function () {
  vCurrentPetId = this.dataset.id;
  $("#delete-modal").modal("show");
});

$("#btn-delete-pet").click(() => handleCrud(DELETE));

// Create Pet
$("#btn-create").click(handleBtnAddPet);

$("#btn-create-pet").click(() => handleCrud(CREATE));

/******** Call Api Function Ajax ********/

// Create Pet
function callApiToCreatePet(paramObj) {
  $.ajax({
    url: `${gBASE_URL_PETS}`,
    method: "POST",
    contentType: "application/json",
    data: JSON.stringify(paramObj),
    success: function (res) {
      console.log(res);
      handleSuccessState(CREATE);
    },
  });
}

// Update Pet
function callApiToUpdatePet(paramObj) {
  $.ajax({
    url: `${gBASE_URL_PETS}${vCurrentPetId}`,
    method: "PUT",
    contentType: "application/json",
    data: JSON.stringify(paramObj),
    success: function (res) {
      console.log(res);
      handleSuccessState(UPDATE);
    },
  });
}

// Delete Pet
function callApiToDeletePet() {
  $.ajax({
    url: `${gBASE_URL_PETS}${vCurrentPetId}`,
    method: "DELETE",
    success: function () {
      handleSuccessState(DELETE);
    },
  });
}

/******** CRUD Function ********/

function handleCrud(paramMethod) {
  if (paramMethod === DELETE) {
    return callApiToDeletePet();
  }

  var vPetObj = getEmptyPetObject();

  getDataFromModal(paramMethod, vPetObj);

  if (!validateInput(vPetObj)) return;

  console.log(vPetObj);
  paramMethod === CREATE
    ? callApiToCreatePet(vPetObj)
    : callApiToUpdatePet(vPetObj);
}

function getEmptyPetObject() {
  return {
    name: "",
    type: "",
    description: "",
    price: "",
    promotionPrice: "",
    imageUrl: "",
    discount: null,
  };
}

/******** Functions help Create Pet ********/
function handleBtnAddPet() {
  $("#create-modal").modal(`show`);
}

/******** Functions help Update Pet ********/
function handleUpdateBtn(paramPet) {
  $("#update-modal").modal("show");
  vCurrentPetId = paramPet.id;
  populateDataToModalUpdate(paramPet);
}

function populateDataToModalUpdate(paramObj) {
  $("#inp-update-name").val(paramObj.name);
  $("#inp-update-type").val(paramObj.type);
  $("#inp-update-desc").val(paramObj.description);
  $("#inp-update-img-url").val(paramObj.imageUrl);
  $("#inp-update-price").val(paramObj.price);
  $("#inp-update-pro-price").val(paramObj.promotionPrice);
  // Show Img
  $("#img-update-pet").attr("src", paramObj.imageUrl);
}

/******** Support Functions to Create and Update Pet ********/
function getDataFromModal(paramMethod, paramObj) {
  paramObj.name = $(`#inp-${paramMethod}-name`).val().trim();
  paramObj.type = $(`#inp-${paramMethod}-type`).val().trim();
  paramObj.description = $(`#inp-${paramMethod}-desc`).val().trim();
  paramObj.imageUrl = $(`#inp-${paramMethod}-img-url`).val().trim();
  paramObj.price = $(`#inp-${paramMethod}-price`).val().trim();
  paramObj.promotionPrice = $(`#inp-${paramMethod}-pro-price`).val().trim();
}

function validateInput(paramObj) {
  var vPrice = parseInt(paramObj.price);
  var vPromotionPrice = parseInt(paramObj.promotionPrice);

  if (
    !paramObj.name ||
    !paramObj.type ||
    !paramObj.description ||
    !paramObj.imageUrl ||
    !vPrice ||
    !vPromotionPrice
  ) {
    alert(`Please fill out all input!`);
  } else if (vPrice <= 0) {
    alert("Pet Price must be greater than 0!");
  } else if (vPromotionPrice <= 0 || vPromotionPrice >= vPrice) {
    alert("Discount Price Must be smaller than real Price!");
  } else {
    return true;
  }
  return false;
}

function handleSuccessState(paramMethod) {
  alert(`${paramMethod.toUpperCase()} Pet Successfully!`);
  $(`#${paramMethod}-modal`).modal("hide");
  loadPet();
}

// Slider
let vSliderValue = [0, 10000];
$("#slider-range").slider({
  range: true,
  values: [0, 10000],
  min: 0,
  max: 10000,
  step: 1,
  slide: function (event, ui) {
    $("#min-price").html(ui.values[0]);

    $("#max-price").html(ui.values[1]);
    vSliderValue = ui.values;
  },
});

// Filter Pet

$("#btn-filter").click(() => {
  loadPet();
});
